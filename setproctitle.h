/*
  Copyright (C) 2004-2008  Dmitry V. Levin <ldv@altlinux.org>

  The setproctitle library interface.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef SETPROCTITLE_H_
#define SETPROCTITLE_H_

#ifdef	__cplusplus
extern "C" {
#endif

extern int setproctitle (const char *fmt, ...)
	__attribute__ ((__format__ (__printf__, 1, 2)));

#ifdef	__cplusplus
}
#endif

#endif /* SETPROCTITLE_H_ */
