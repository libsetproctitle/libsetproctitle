#
# Copyright (C) 2001-2006  Dmitry V. Levin <ldv@altlinux.org>
#
# Makefile for the setproctitle project.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#

PROJECT = setproctitle
VERSION = $(shell sed '/^Version: */!d;s///;q' setproctitle.spec)
MAJOR = 0

SHLIB_NAME = lib$(PROJECT).so
SONAME = $(SHLIB_NAME).$(MAJOR)
SHLIB = $(SHLIB_NAME).$(VERSION)
MAP = $(PROJECT).map

TARGETS = $(SHLIB) $(SONAME) $(SHLIB_NAME)

INSTALL = install
libdir = /usr/lib
includedir = /usr/include
mandir = /usr/share/man
man3dir = $(mandir)/man3
DESTDIR =

WARNINGS = -W -Wall -Waggregate-return -Wcast-align -Wconversion \
	-Wdisabled-optimization -Wmissing-declarations \
	-Wmissing-format-attribute -Wmissing-noreturn \
	-Wmissing-prototypes -Wpointer-arith -Wredundant-decls \
	-Wshadow -Wstrict-prototypes -Wwrite-strings
CPPFLAGS = -std=gnu99 $(WARNINGS) -D_GNU_SOURCE
CFLAGS = $(RPM_OPT_FLAGS) -fPIC
LDFLAGS = -shared -nostartfiles -Wl,-soname,$(SONAME),--version-script,$(MAP),-z,defs,-z,now,-stats

all: $(TARGETS)

$(SONAME) $(SHLIB_NAME): $(SHLIB)
	ln -sf $< $@

$(SHLIB): $(PROJECT).o $(MAP)
	$(LINK.o) $< $(OUTPUT_OPTION)

$(PROJECT).o: $(PROJECT).c setproctitle.h

install:
	$(INSTALL) -pD -m755 $(SHLIB) $(DESTDIR)$(libdir)/$(SHLIB)
	$(INSTALL) -pD -m644 $(PROJECT).3 $(DESTDIR)$(man3dir)/$(PROJECT).3
	$(INSTALL) -pD -m644 $(PROJECT).h $(DESTDIR)$(includedir)/$(PROJECT).h
	ln -s $(SHLIB) $(DESTDIR)$(libdir)/$(SONAME)
	ln -s $(SHLIB) $(DESTDIR)$(libdir)/$(SHLIB_NAME)

clean:
	$(RM) $(TARGETS) core *.o *~
